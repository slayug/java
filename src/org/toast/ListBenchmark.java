package org.toast;

import java.util.ArrayList;
import java.util.LinkedList;

public class ListBenchmark {

    public static void main(String[] args) {

        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        LinkedList<Integer> linkedList = new LinkedList<Integer>();

        // ArrayList add
        long startTime = System.nanoTime();

        for (int i = 0; i < 100000; i++) {
            arrayList.add(i);
        }
        long endTime = System.nanoTime();
        long duration = endTime - startTime;
        System.out.println("ArrayList add:  " + duration);

        // LinkedList add
        startTime = System.nanoTime();

        for (int i = 0; i < 100000; i++) {
            linkedList.add(i);
        }
        endTime = System.nanoTime();
        duration = endTime - startTime;
        System.out.println("LinkedList add: " + duration);

        // ArrayList get
        startTime = System.nanoTime();

        for (int i = 0; i < 10000; i++) {
            arrayList.get(i);
        }
        endTime = System.nanoTime();
        duration = endTime - startTime;
        System.out.println("ArrayList get:  " + duration);

        // LinkedList get
        startTime = System.nanoTime();

        for (int i = 0; i < 10000; i++) {
            linkedList.get(i);
        }
        endTime = System.nanoTime();
        duration = endTime - startTime;
        System.out.println("LinkedList get: " + duration);


        // ArrayList remove
        startTime = System.nanoTime();

        for (int i = 9999; i >= 0; i--) {
            arrayList.remove(i);
        }
        endTime = System.nanoTime();
        duration = endTime - startTime;
        System.out.println("ArrayList remove:  " + duration);


        // LinkedList remove
        startTime = System.nanoTime();

        for (int i = 9999; i >= 0; i--) {
            linkedList.remove(i);
        }
        endTime = System.nanoTime();
        duration = endTime - startTime;
        System.out.println("LinkedList remove: " + duration);

    }


    // great resource: https://www.netjstech.com/2015/08/how-arraylist-works-internally-in-java.html

    /**
    ArrayList in Java is a Resizable-array implementation of the List interface.
    Internally ArrayList class uses an array of Object class to store its elements.
    When initializing an ArrayList you can provide initial capacity then the array would be of the size provided as initial capacity.
    If initial capacity is not specified then default capacity is used to create an array. Default capacity is 10.
    When an element is added to an ArrayList it first verifies whether it can accommodate the new element or it needs
     to grow, in case capacity has to be increased then the new capacity is calculated which is 50% more than the old
     capacity and the array is increased by that much capacity.
    When elements are removed from an ArrayList space created by the removal of an element has to be filled in the
     underlying array. That is done by Shifting any subsequent elements to the left.
     */

}
