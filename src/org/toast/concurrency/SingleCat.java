package org.toast.concurrency;

import org.toast.CatService;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.IntStream;

public class SingleCat {

    public static void main(String[] args) {
        ExecutorService service = null;

        var catService = CatService.INSTANCE;

        catService.downloadCatImage(catService.getCatByName("404"));

        try {
            service = Executors.newSingleThreadExecutor();

            System.out.println("let the show begin");

            service.execute(() -> System.out.println("Counting cats"));
            service.execute(() -> {
                IntStream.range(1, 4).forEach(index -> System.out.println("Cat n°" + index));
            });

            service.execute(() -> System.out.println("Counting cats"));
            service.execute(() -> System.out.println("this is the end."));


        } finally {
            if (service != null) service.shutdown();
        }

        //TODO change the singleThreadExecutor()
    }

    public static void future() {
        Runnable task = () -> {
            ConcurrencyUtils.simulateWaiting();
            System.out.println("This is thread " + Thread.currentThread().getName());
        };

        ExecutorService executor = Executors.newFixedThreadPool(4);

        try {
            List<Future<?>> futureList = new ArrayList<>();
            IntStream.range(0, 10).forEach(cnt -> futureList.add(executor.submit(task)));

           futureList.forEach(future -> {
               try {
                   System.out.println("Result: " + future.get(1, TimeUnit.SECONDS));
               } catch (InterruptedException e) {
                   e.printStackTrace();
               } catch (ExecutionException e) {
                   e.printStackTrace();
               } catch (TimeoutException e) {
                   e.printStackTrace();
               }
           });
        } finally {
            executor.shutdown();
        }









        //TODO retourner quelque chose à chaque future
        //TODO ajouter une exception dans la task
        //TODO comment mieux implémenter cette notion de quand tout est terminé ?

    }



    // CompletableFuture
    public static void completableFuture() {

        CompletableFuture<?> future = CompletableFuture.runAsync(() -> {
            try {
                System.out.println("Running asynchronous task in parallel");
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException ex) {
                throw new IllegalStateException(ex);
            }
        });









        //TODO OK mais on a déjà fais ça, il y a rien de nouveau ici, et puis il est où executorService ?
        //TODO Ajouter quelque chose en retour, non mais toujours pareil on l'a déjà fait
        //TODO thenSmth, ah oui on a pas fait ça c'est nice

    }




}
