package org.toast.concurrency;

public final class ConcurrencyUtils {

    private ConcurrencyUtils() {

    }

    public static void simulateWaiting() {
        var waitingTime = (int) (Math.random() * 1200);
        try {
            Thread.sleep(waitingTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
