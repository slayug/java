package org.toast;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class Pipeline<T> {


    private final List<Predicate<T>> predicates;
    private final List<Supplier<T>> suppliers;
    private final List<T> elements;
    private final List<Consumer> consumers;

    public Pipeline(List<T> elements) {
        this.elements = elements;
        predicates = new ArrayList<>();
        suppliers = new ArrayList<>();
        consumers = new ArrayList<>();
    }



    public void forEach(Consumer<T> consumer) {
    }


    public void apply() {
    }


}
