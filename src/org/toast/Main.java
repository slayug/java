package org.toast;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {

        // #1 - Consumer, montrer l'exemple avec List<T>.forEach();

        // #2 - Usage avec les Optional

        // #3 - Stream

        // #4 - Concurrency
        collect();
    }


    public static void terminal() {

        List<String> list = new ArrayList<>();
        list.add("Jim");
        list.add("Jimmy");

        Stream<String> stream = list.stream();
        list.add("toast");

        // Question ?
        System.out.println("count: " + stream.count());

    }

    public static void collect() {
        List<String> list = new ArrayList<>();
        list.add("Jim");
        list.add("Jimmy");
        list.add("Toast");

        System.out.println(list.stream().collect(Collectors.joining(", ")));
    }
}
