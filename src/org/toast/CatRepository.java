package org.toast;

import java.util.HashMap;
import java.util.Map;

public enum CatRepository {
    INSTANCE;

    private static final Map<String, Cat> CATS = new HashMap<>();

    static {
        CATS.put("100", new Cat("100")); 
        CATS.put("101", new Cat("101")); 
        CATS.put("200", new Cat("200")); 
        CATS.put("201", new Cat("201")); 
        CATS.put("202", new Cat("202")); 
        CATS.put("204", new Cat("204")); 
        CATS.put("206", new Cat("206")); 
        CATS.put("207", new Cat("207")); 
        CATS.put("300", new Cat("300")); 
        CATS.put("301", new Cat("301")); 
        CATS.put("302", new Cat("302")); 
        CATS.put("303", new Cat("303")); 
        CATS.put("304", new Cat("304")); 
        CATS.put("305", new Cat("305")); 
        CATS.put("307", new Cat("307")); 
        CATS.put("400", new Cat("400")); 
        CATS.put("401", new Cat("401")); 
        CATS.put("402", new Cat("402")); 
        CATS.put("403", new Cat("403")); 
        CATS.put("404", new Cat("404")); 
        CATS.put("405", new Cat("405")); 
        CATS.put("406", new Cat("406")); 
        CATS.put("408", new Cat("408")); 
        CATS.put("409", new Cat("409")); 
        CATS.put("410", new Cat("410")); 
        CATS.put("411", new Cat("411")); 
        CATS.put("412", new Cat("412")); 
        CATS.put("413", new Cat("413")); 
        CATS.put("414", new Cat("414")); 
        CATS.put("415", new Cat("415")); 
        CATS.put("416", new Cat("416")); 
        CATS.put("417", new Cat("417")); 
        CATS.put("418", new Cat("418")); 
        CATS.put("420", new Cat("420")); 
        CATS.put("421", new Cat("421")); 
        CATS.put("422", new Cat("422")); 
        CATS.put("423", new Cat("423")); 
        CATS.put("424", new Cat("424")); 
        CATS.put("425", new Cat("425")); 
        CATS.put("426", new Cat("426")); 
        CATS.put("429", new Cat("429")); 
        CATS.put("431", new Cat("431")); 
        CATS.put("444", new Cat("444")); 
        CATS.put("450", new Cat("450")); 
        CATS.put("451", new Cat("451")); 
        CATS.put("499", new Cat("499")); 
        CATS.put("500", new Cat("500")); 
        CATS.put("501", new Cat("501")); 
        CATS.put("502", new Cat("502")); 
        CATS.put("503", new Cat("503")); 
        CATS.put("504", new Cat("504")); 
        CATS.put("506", new Cat("506")); 
        CATS.put("507", new Cat("507")); 
        CATS.put("508", new Cat("508")); 
        CATS.put("509", new Cat("509")); 
        CATS.put("510", new Cat("510")); 
        CATS.put("511", new Cat("511")); 
        CATS.put("599", new Cat("599"));
    }

    public Cat getCatByName(String name) {
        return CATS.get(name);
    }

}
