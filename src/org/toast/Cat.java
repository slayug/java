package org.toast;

public class Cat {

    private static final String BASE_URL = "https://http.cat/";
    private static final String DOWNLOAD_PATH = "/tmp/cats/%s.jpg";

    private final String name;

    public Cat(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return BASE_URL + this.name;
    }

    public String getDownloadPath() {
        return String.format(DOWNLOAD_PATH, this.getName());
    }

}
