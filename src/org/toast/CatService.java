package org.toast;

import org.toast.concurrency.ConcurrencyUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

public enum CatService {
    INSTANCE;

    public Cat getCatByName(String name) {
        ConcurrencyUtils.simulateWaiting();
        return CatRepository.INSTANCE.getCatByName(name);

    }

    public void downloadCatImage(Cat cat) {
        try(InputStream in = new URL(cat.getUrl()).openStream()){
            Files.copy(in, Paths.get(cat.getDownloadPath()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String downloadCatImageAndGetPath(Cat cat) {
        try(InputStream in = new URL(cat.getUrl()).openStream()){
            Files.copy(in, Paths.get(cat.getDownloadPath()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return cat.getDownloadPath();
    }

}
